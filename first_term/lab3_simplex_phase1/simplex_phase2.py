import numpy as np  
import inverse_matrix
import math
import collections
import logging


SOLUTION_FOUND_STATUS_RES = 0
OBJECTIVE_FUNC_IS_UNBOUNDED_ABOVE = 1

log = logging.getLogger('phase2')
log.setLevel(logging.DEBUG)
noop = logging.NullHandler()
log.addHandler(noop)
# log.addHandler(logging.StreamHandler())


def can_improve(delta, non_basis_indices, eps=10**(-6)):
	log.debug('{0}'.format(np.take(delta, non_basis_indices)))
	log.debug('{0}'.format(any(x<0. for x in np.take(delta, non_basis_indices))))
	return any(x<0. for x in np.take(delta, non_basis_indices))

def get_pivot_index(delta, non_basis_indices, eps=10**(-6)):
	non_basis_delta = np.take(delta, non_basis_indices)
	below_zero_delta = non_basis_delta[non_basis_delta < eps]
	argmin = np.where(abs(delta - below_zero_delta.min())<=eps)[0]   #return tuple
	argmin = argmin[0]  # choose first from possible options
	# print ('arg', argmin_non_basis[1]) 
	# print (argmin)
	return argmin # pair row-col, delta is vector(but here transposed) so we interested only in col


def objective_func_is_bounded_above(z):
	return any(z_i>0. for z_i in z)

def find_steps(x, z, J_b):
	# print("FIND STEPS")
	z_range = range(0, len(z))
	# for i in z_range:
	# 	print('i', i)
	# 	print ('z[i]', z[i])
	# 	if z[i]<=0.:
	# 	    print (math.inf )
	# 	else:
	# 		print('x', x)
	# 		print('J_b',J_b[i])
	# 		print(x[J_b[i]]/z[i])
	teta = [math.inf if z[i]<=0. else x[J_b[i]]/z[i] for i in z_range]
	return teta


def calc_potentials_vector(basis_c, inv_basis_A):
	return np.dot(basis_c.transpose(), inv_basis_A).transpose()


def calc_evaluation_vector(u, A, c):
	delta = u.transpose()*A -c.transpose()
	delta = np.squeeze(np.asarray(delta))
	return delta 


def phase2(c, A, b, x, basis_indices, eps=10**(-6)):
	# print ('c', c)
	# print('A', A)
	# # print(b)
	# print (x)
	# indices = [i for i in range(0, len(A))]
	# 

	indices = [i for i in range(len(x))]

	basis_A = A[:,basis_indices]
	inv_basis_A = np.linalg.inv(basis_A)

#iteration
	while True:
		non_basis_indices = [i for i in indices if i not in basis_indices]
		log.debug('basis indices {0}'.format(basis_indices))
		log.debug('non_basis_indices {0}'.format(non_basis_indices))

		log.debug('A_basis\n{0}'.format(basis_A))
		log.debug('A_basis_inv\n{0}'.format(inv_basis_A))

		basis_c = np.take(c, basis_indices).transpose()
		u = calc_potentials_vector(basis_c, inv_basis_A)
		# print ('u', u)
		delta = calc_evaluation_vector(u, A, c)
		log.debug('delta {0}'.format(delta))
		if not can_improve(delta, non_basis_indices, eps):
			return SOLUTION_FOUND_STATUS_RES, x, basis_indices

		# FOR THINKING: WHY IF DELTA<0 THEN WE CAN IMPROVE
		j0 = get_pivot_index(delta, non_basis_indices, eps)   #non-basis index where delta < 0		
		log.debug('chosen non-basis pivot index {0}'.format(j0))
		z = np.dot(inv_basis_A, A[:,j0])    # one component for each basis index
		log.debug('z {0}'.format(z))

		if not objective_func_is_bounded_above(z):
			return OBJECTIVE_FUNC_IS_UNBOUNDED_ABOVE, None, None

		teta = find_steps(x, z, basis_indices)
		log.debug('tetas {0}'.format(teta))
		teta_0 = min(teta)
		log.debug('teta0 {0}'.format(teta_0))
		s = teta.index(teta_0)   #index_of_min_step - basis index for replace
		log.debug('index of min step (teta0) (basis index for replace) {0}'.format(s))


		basis_elem_for_replace_ind = basis_indices[s]   # basis elem for replace with non-basis
		basis_indices[s] = j0    #replace basis index with non-basis



		new_x = np.zeros(len(x))
		for i, j_i in enumerate(basis_indices):
			new_x[j_i] = x[j_i] - teta_0*z[i]

		new_x[basis_indices[s]] = teta_0

		log.debug('{0} {1}'.format(new_x, basis_indices))
		log.debug('-----------------------------------------')

		inv_basis_A = inverse_matrix.inv(basis_A, inv_basis_A, A[:,j0], s)
		basis_A = A[:,basis_indices]

		x = new_x


def example_0():
	A=np.matrix([
		[0, 1, 4, 1, 0, -3, 5, 0], 
		[1, -1, 0, 1, 0, 0, 1, 0],
		[0, 7, -1, 0, -1, 3, 8, 0], 
		[1, 1, 1, 1, 0, 3, -3, 1]
		])  #np.zeros((dim, dim))
	# inversed_A = np.matrix([[1, 1, 0], [0, 1, 0], [0, 0, 1]])
	x = np.matrix([[4], [0], [0], [6], [2], [0], [0], [5]])
	c = np.matrix([[-5], [-2], [3], [-4], [-6], [0], [-1], [-5]])
	b = np.matrix([[6], [10], [-2], [15]])
	J_b = [0, 3, 4, 7]
	# J_n = [0, 1]
	#J = [i for i in range(len(x))]
	print (phase2(c, A, b, x, J_b))

def example_3():
	A=np.matrix([
		[0., -1., 1., -7.5, 0., 0., 0., 2.], 
		[0., 2., 1., 0., -1., 3., -1.5, 0.],
		[1., -1., 1., -1., 0., 3., 1., 1.]
		])  #np.zeros((dim, dim))
	# inversed_A = np.matrix([[1, 1, 0], [0, 1, 0], [0, 0, 1]])
	x = np.matrix([[4.], [0.], [6.], [0], [4.5], [0.], [0.], [0]])
	c = np.matrix([[-6.], [-9.], [-5.], [2.], [-6.], [0.], [1.], [3.]])
	b = np.matrix([[6.], [1.5], [10]])
	J_b = [0, 2, 4]
	# J_n = [0, 1]
#	J = [i for i in range(len(x))]
	print (phase2(c, A, b, x, J_b))


def laba_example():
	eps = 10^(-6)
	A=np.matrix([[-1, 1, 1, 0, 0], [1, 0, 0, 1, 0],[0, 1, 0, 0, 1]])  #np.zeros((dim, dim))
	# inversed_A = np.matrix([[1, 1, 0], [0, 1, 0], [0, 0, 1]])
	x = np.matrix([[0], [0], [1], [3], [2]])
	c = np.matrix([[1], [1], [0], [0], [0]])
	b = np.matrix([[1], [3], [2]])
	J_b = [2,3,4]
	J_n = [0, 1]
#	J = [0,1,2,3,4]
	print (phase2(c, A, b, x, J_b))

if __name__ == '__main__':
	#laba_example()
	example_3()
	# example_0()

#!!!!!!!!!!!a[:, np.newaxis]





