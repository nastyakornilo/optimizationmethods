import numpy as np
from numpy.linalg import inv
import itertools


def find_y(costs_vector_basis, inv_A_basis):
    return (np.dot(costs_vector_basis.T,inv_A_basis)).T

def find_deltas(y, A, costs_vector):
    return (np.dot(y.T, A) - costs_vector.T).T

def split_non_basis_indices(nonbasis_indices, deltas):
    negative = [j for j in nonbasis_indices if deltas[j] < 0]
    positive = [j for j in nonbasis_indices if deltas[j] >= 0]
    return negative, positive

def find_pseudoplan(
    J_nonbasis_plus,
    J_nonbasis_minus,
    low_borders,
    high_borders,
    A,
    inv_A_basis,
    b,
    J_basis
    ):
    pseudoplan =np.zeros(A.shape[1])[np.newaxis, :].T
    for j in J_nonbasis_plus:
        pseudoplan[j] = low_borders[j]
    for j in J_nonbasis_minus:
        pseudoplan[j] = high_borders[j]

    sum_ =sum([pseudoplan.flat[j]*A[:,j] for j in itertools.chain(
                                                                J_nonbasis_plus,
                                                                J_nonbasis_minus
                                                                )])
    pseudoplan[J_basis] = np.dot(inv_A_basis, (b - sum_))
    return pseudoplan

def find_leaving_index(
    low_borders, high_borders, pseudoplan
    ):
    for i, (low, pseudoplan_i, high) in enumerate(
        zip(low_borders, pseudoplan, high_borders)):
        if low>pseudoplan_i or pseudoplan_i>high:
            return i

# def find_leaving_index_pos(low_borders, high_borders, pseudoplan, J_basis):
#     value = find_leaving_index(
#         low_borders, high_borders, pseudoplan
#         )   #basis index
#     return np.where(J_basis == value)[0]

def find_entering_index(
    low, high, pseudoplan, leaving_index,
    A, A_inv_basis, J_nonbasis,J_nonbasis_plus, J_nonbasis_minus,
    leaving_index_basis_pos, deltas
    ):
    def find_nu():
        nu = np.zeros(len(pseudoplan))    #number of cilumns in A
        nu[leaving_index] = 1 if low[leaving_index]>pseudoplan.flat[leaving_index] else -1
        delta_y = (nu[leaving_index]*A_inv_basis[leaving_index_basis_pos]).T
        for j in J_nonbasis:
            nu[j] = np.dot(delta_y.T, A[:,j])
        return nu

    def find_sigmas(nu):
        non_basis_len = len(J_nonbasis)
        sigmas = [float('infinity') for i in range(non_basis_len)]
        for pos in range(non_basis_len):
            j = J_nonbasis[pos]
            if (j in J_nonbasis_plus and nu[j]<0):
                sigmas[pos] = -deltas.flat[j]/nu[j]
            elif (j in J_nonbasis_minus and nu[j]>0):
                sigmas[pos] = -deltas.flat[j]/nu[j]
        return sigmas

    nu = find_nu()
    # print ('nu', nu)
    sigmas = find_sigmas(nu)
    # print ('sigmas', sigmas)
    sigma0 = min(sigmas)
    if sigma0 == float('infinity'):
        raise Exception('Problem is infeasible')

    entering_index = J_nonbasis[np.where(sigmas==sigma0)[0]]
    return entering_index

def dual_simplex(A, costs_vector, b, low_borders, high_borders, J_basis):
    while True:
        print ('J_basis', J_basis)
        J_nonbasis = np.array([i for i in range(A.shape[1]) if i not in J_basis])
        print ('J_nonbasis', J_nonbasis)
        inv_A_basis = inv(A[:,J_basis])
        y = find_y(costs_vector[J_basis], inv_A_basis)
        print ('y: ', y)
        deltas = find_deltas(y, A, costs_vector)
        print ('deltas: ', deltas)
        J_nonbasis_minus, J_nonbasis_plus = split_non_basis_indices(J_nonbasis, deltas.flat)
        print ('J_n+: ',J_nonbasis_plus, 'J_n-: ', J_nonbasis_minus)
        pseudoplan = find_pseudoplan(
            J_nonbasis_plus, J_nonbasis_minus,
            low_borders, high_borders,
            A, inv_A_basis, b, J_basis
            )
        print ('pseudoplan', pseudoplan)
        leaving_index = find_leaving_index(low_borders, high_borders, pseudoplan)
        print ('leaving_index', leaving_index)
        if leaving_index is None:
            return pseudoplan, J_basis
        leaving_index_basis_pos = np.where(J_basis == leaving_index)[0][0]
        print ('leaving_index_basis_pos', leaving_index_basis_pos)
        entering_index = find_entering_index(
            low_borders, high_borders, pseudoplan, leaving_index, A,
            inv_A_basis, J_nonbasis, J_nonbasis_plus, J_nonbasis_minus,
            leaving_index_basis_pos, deltas
            )
        J_basis[leaving_index_basis_pos] = entering_index
        print ('entering_index', entering_index)
        pos = np.where(J_nonbasis == entering_index)[0][0]
        print ('entering index pos', pos)
        J_nonbasis[pos] = leaving_index
        print ('new J_basis', J_basis)


def example():
    A = np.matrix(
        [
            [2, 1, -1, 0, 0, 1],
            [1, 0, 1, 1, 0, 0],
            [0, 1, 0, 0, 1, 0]
        ]
    )
    b = np.matrix([[2],[5],[0]])
    c = np.array([3, 2, 0, 3, -2, -4])
    low = np.array([0, -1, 2, 1, -1, 0])
    high = np.array([2, 4, 4, 3, 3, 5])
    J_basis = np.array([3,4,5])
    x, J_b = (dual_simplex(A, c, b, low, high, J_basis))
    print ('X')
    print(x)
    print('OBJECT FUNC', np.dot(c, x))

def task_1():
    A = np.matrix([
    [1, -5, 3, 1, 0, 0],
    [4, -1, 1, 0, 1, 0],
    [2, 4, 2, 0, 0, 1]
    ])

    b = np.matrix([[-7], [22], [30]])  #[np.newaxis, :].T

    c = np.matrix(
        [[7], [-2], [6], [0], [5], [2]])   #[np.newaxis, :].T

    d_lower = np.array([2, 1, 0, 0, 1, 1])

    d_upper = np.array([6, 6, 5, 2, 4, 6])
    J_b = np.array([1,2,3])

    x, J_b = (dual_simplex(A, c, b, d_lower, d_upper, J_b))
    print ('X')
    print(x)
    print('OBJECT FUNC', np.dot(c.T, x))

def task_2():
    A = np.matrix([
    [1., 0., 2., 2., -3., 3.],
    [0., 1., 0., -1., 0., 1.],
    [1., 0., 1., 3., 2., 1.]
    ])

    b = np.matrix([[15.], [0.],[ 13.]])  #[np.newaxis, :].T

    c = np.matrix(
        [[3.], [0.5], [4.], [4.], [1.],[5.]])

    d_lower = np.array([0., 0., 0., 0., 0., 0.])

    d_upper = np.array([3., 5., 4., 3., 3., 4.])
    J_b = np.array([3, 4, 5])

    x, J_b = (dual_simplex(A, c, b, d_lower, d_upper, J_b))
    print (c.shape)
    print ('X')
    print(x)
    print('OBJECT FUNC', np.dot(c.T, x))

def task_3():
    A = np.matrix([
    [1., 0., 0., 12., 1., -3., 4., -1.],
    [0., 1., 0., 11., 12., 3., 5., 3.],
    [0., 0., 1., 1., 0., 22., -2., 1.]
    ])

    b = np.matrix([[40.], [107.], [61.]])

    c = np.matrix(
        [[2.], [1.],[-2.], [-1.], [4.], [-5.], [5.], [5.]])

    d_lower = np.array([0., 0., 0., 0., 0., 0., 0., 0.])

    d_upper = np.array([3., 5., 5., 3., 4., 5., 6., 3.])
    J_b = np.array([3, 5, 6])

    x, J_b = (dual_simplex(A, c, b, d_lower, d_upper, J_b))
    print ('X')
    print(x)
    print('OBJECT FUNC', np.dot(c.T, x))
#
def task_4():
    A = np.matrix([
    [1., -3., 2., 0., 1., -1., 4., -1., 0.],
    [1., -1., 6., 1., 0., -2., 2., 2., 0.],
    [2., 2., -1., 1., 0., -3., 8., -1., 1.],
    [4., 1., 0., 0., 1., -1., 0., -1., 1.],
    [1., 1., 1., 1., 1., 1., 1., 1., 1.]
    ])

    b = np.matrix([[3.], [9.], [9.], [5.], [9.]])

    c = np.matrix(
        [[-1.], [5.], [-2.], [4.], [3.], [1.], [2.], [8.], [3.]])

    d_lower = np.array([0., 0., 0., 0., 0., 0., 0., 0., 0.])

    d_upper = np.array([5., 5., 5., 5., 5., 5., 5., 5., 5.])
    J_b = np.array([3, 2, 1, 4, 5])

    x, J_b = (dual_simplex(A, c, b, d_lower, d_upper, J_b))
    print ('X')
    print(x)
    print('OBJECT FUNC', np.dot(c.T, x))

def task_5():
    A = np.matrix([
    [1, 7, 2, 0, 1, -1, 4],
    [0, 5, 6, 1, 0, -3, -2],
    [3, 2, 2, 1, 1, 1, 5]
    ])

    b = np.matrix([[1.], [4.], [7.]])

    c = np.matrix(
        [[1.], [2.], [1.], [-3.], [3.], [1.], [0.]])

    d_lower = np.array([-1, 1, -2, 0, 1, 2, 4])

    d_upper = np.array([3, 2, 2, 5, 3, 4, 5])
    J_b = np.array([2, 1, 0])
    x, J_b = (dual_simplex(A, c, b, d_lower, d_upper, J_b))
    print ('X')
    print(x)
    print('OBJECT FUNC', np.dot(c.T, x))

if __name__ == '__main__':
    task_1()
