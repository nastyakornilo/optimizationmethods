# from scipy.optimize import linprog
import math
import numpy as np
from basic_simplex import  basic_phase
from initial_simplix import initial_phase
import copy
# class OptimizeStatus(object):
#     success = 0
#     iterations_limit_reached = 1
#     problem_is_infeasible = 2
#     problem_is_unbounded = 3

class IntegerLpProblem(object):
    def __init__(self, c=None, A=None, b=None):
        self.A = A
        self.b = b
        self.c = c

    def __str__(self):
        return str(self.__dict__)

class OptimizeResult(object):
    def __init__(self, x, basis, c):
        self.x = x
        self.basis = basis
        self.func = np.dot(c.T, x)

    def __str__(self):
        return str({'func': self.func, 'x':self.x})


def linprog(c, A_eq, b_eq, eps):
    # x, basis =  (A_eq, list(b_eq.flat))

    A, b, c, x_b, J_b = initial_phase(A_eq, b_eq, c)

    basic_phase(A, b, c, x_b, J_b)

    return OptimizeResult(x_b, J_b, c)#basic_phase(A, b, c, x_b, J_b)phase2(c, A_eq, b_eq, x, basis, eps)


def remove_trash_eq(A, b, basis, native_vars_count):
    is_index_of_artificial = lambda j: False if j < native_vars_count else True
    index_of_rows_to_remove = []
    for j in sorted(reversed(basis)):
        if not is_index_of_artificial(j):
            break
        index_of_rows_to_remove.append(j)
        for i, row in enumerate(A[:, j:]):
            # achieve 0 at A[i][j]
            multiplier = A[i][j] // A[j, j]

            A[i] = A[i] - A[j] * multiplier
            b[i] = b[i] - b[j] * multiplier
    for j in index_of_rows_to_remove:  #indexes are sorted from the largest to  the lowest
        del A[j]
        del b[j]
    return A, b


def solve_by_gomary(problem, eps=10**(-10), iters_until_remove_trash_eq=100):
    # first = True
    native_vars_count = problem.A.shape[1]
    while True:
        for iter in range(iters_until_remove_trash_eq):
            optimize_res = linprog(c=-problem.c, A_eq=problem.A, b_eq=problem.b, eps=eps)
            # if first:
            #     optimize_res.J_basis=[0,1,2]
            #     first = False

            non_integer_pos_in_x = find_non_integer_component_pos(optimize_res.x[:native_vars_count], eps)
            if non_integer_pos_in_x is None:
                optimize_res = OptimizeResult(optimize_res.x[:native_vars_count], optimize_res.basis, problem.c[:native_vars_count])
                return optimize_res

            new_constraint = create_new_constraint(problem, optimize_res, non_integer_pos_in_x)
            A_row, b = new_constraint

            A = np.zeros((problem.A.shape[0], problem.A.shape[1]+1))
            A[:, :-1] = copy.deepcopy(problem.A)
            problem.A = copy.deepcopy(A)


            problem.A = np.concatenate((problem.A, [A_row]))
            problem.b = np.append(problem.b, b)
            problem.c = np.append(problem.c, 0)
            problem.A, problem.b = remove_trash_eq(problem.A,problem.b, optimize_res.basis, native_vars_count)


def decimal(x):
    return x-math.floor(x)


def create_new_constraint(problem, optimize_res, non_integer_pos_in_x):
    A_basis = problem.A[:,optimize_res.basis]
    A_basis_inv = np.linalg.inv(A_basis)
    C = np.dot(A_basis_inv, problem.A)

    pos_in_basis = list(optimize_res.basis).index(non_integer_pos_in_x)  # position in basis of non integer component index in x

    A_row = list(map(lambda x: -(decimal(x)), C[pos_in_basis]))  #modf split number on decimal and integer part
    A_row.append(1)
    A_row = [a if i not in optimize_res.basis else 0 for i, a in enumerate(A_row)]
    bad_x = optimize_res.x[non_integer_pos_in_x]#np.dot(A_basis_inv, problem.b)[non_integer_pos_in_x]
    b = -decimal(bad_x)
    return A_row, b


def find_non_integer_component_pos(x, eps):
    for i, component in enumerate(x):
        e = abs(math.floor(component) - component)
        if abs(round(component) - component) > eps:
            return i

def example():
    problem = IntegerLpProblem()
    # problem.c = np.array([[3.5, -1, 0, 0, 0]])
    # problem.A = np.array([[5,-1,1,0,0],[-1,2,0,1,0],[-7,2,0,0,1]])
    # problem.b = np.array([[15],[6],[0]])
    problem.c = np.array([3.5, -1, 0, 0, 0])
    problem.A = np.array([[5.,-1,1,0,0],[-1,2,0,1,0],[-7,2,0,0,1]])
    problem.b = np.array([15,6,0])
    print(solve_by_gomary(problem))

def example2():
    problem = IntegerLpProblem()
    # problem.c = np.array([[3.5, -1, 0, 0, 0]])
    # problem.A = np.array([[5,-1,1,0,0],[-1,2,0,1,0],[-7,2,0,0,1]])
    # problem.b = np.array([[15],[6],[0]])
    problem.c = np.array([-1, 1, 0, 0, 0])
    problem.A = np.array([[5.,3,1,0,0],[-1,2,0,1,0],[1,-2,0,0,1]])
    problem.b = np.array([4,3,7])
    print(solve_by_gomary(problem))


def task1():
    problem = IntegerLpProblem()
    # problem.c = np.array([[3.5, -1, 0, 0, 0]])
    # problem.A = np.array([[5,-1,1,0,0],[-1,2,0,1,0],[-7,2,0,0,1]])
    # problem.b = np.array([[15],[6],[0]])
    problem.c = np.array([7, -2, 6, 0, 5, 2])
    problem.A = np.array([[1, -5, 3, 1, 0, 0],
                          [4, -1, 1, 0, 1, 0],
                          [2, 4, 2, 0, 0, 1]])
    problem.b = np.array([-8, 22, 30])
    print(solve_by_gomary(problem))

def task2():
    problem = IntegerLpProblem()
    # problem.c = np.array([[3.5, -1, 0, 0, 0]])
    # problem.A = np.array([[5,-1,1,0,0],[-1,2,0,1,0],[-7,2,0,0,1]])
    # problem.b = np.array([[15],[6],[0]])
    problem.c = np.array([-1, 5, -2, 4, 3, 1, 2, 8, 3])
    problem.A = np.array([[1, -3, 2, 0, 1, -1, 4, -1, 0],
                          [1, -1, 6, 1, 0, -2, 2, 2, 0],
                          [2, 2, -1, 1, 0, -3, 8, -1, 1],
                          [4, 1, 0, 0, 1, -1, 0, -1, 1],
                          [1, 1, 1, 1, 1, 1, 1, 1, 1]])
    problem.b = np.array([3, 9, 9, 5, 9])
    print(solve_by_gomary(problem))


def task3():
    problem = IntegerLpProblem()
    # problem.c = np.array([[3.5, -1, 0, 0, 0]])
    # problem.A = np.array([[5,-1,1,0,0],[-1,2,0,1,0],[-7,2,0,0,1]])
    # problem.b = np.array([[15],[6],[0]])
    problem.c = np.array([2,1,-2,-1,4,-5,5,5])
    problem.A = np.array([[1, 0, 0, 12, 1, -3, 4, -1],
                          [0,1,0,11,12,3,5,3],
                          [0,0,1,1,0,22,-2,1]
                        ])
    problem.b = np.array([40, 107, 61])
    print(solve_by_gomary(problem))

def task4():
    problem = IntegerLpProblem()
    # problem.c = np.array([[3.5, -1, 0, 0, 0]])
    # problem.A = np.array([[5,-1,1,0,0],[-1,2,0,1,0],[-7,2,0,0,1]])
    # problem.b = np.array([[15],[6],[0]])
    problem.c = np.array([2, 1, -2, -1, 4, -5, 5, 5, 1, 2])
    problem.A = np.array([[1,2,3,12,1,-3,4,-1,2,3],
                          [0,2,0,11,12,3,5,3,4,5],
                          [0, 0, 2, 1, 0, 22, -2, 1,6,7]
                          ])
    problem.b = np.array([153, 123, 112])
    print(solve_by_gomary(problem))

def task9():
    problem = IntegerLpProblem()
    # problem.c = np.array([[3.5, -1, 0, 0, 0]])
    # problem.A = np.array([[5,-1,1,0,0],[-1,2,0,1,0],[-7,2,0,0,1]])
    # problem.b = np.array([[15],[6],[0]])
    problem.c = np.array([-1, 5, -2, 4,3,1,2,8,3])
    problem.A = np.array([[1, -3, 2, 0, 1, -1, 4, -1, 0],
                          [1, -1, 6, 1, 0, -2, 2, 2,0],
                          [2, 2, -1, 1, 0, -3, 2, -1, 1],
                          [4,1,0,0,1,-1,0,-1,1],
                          [1,1,1,1,1,1,1,1,1]
                          ])
    problem.b = np.array([3,9,9,5,9])
    print(solve_by_gomary(problem))

if __name__ == '__main__':
    # example()
    task9()