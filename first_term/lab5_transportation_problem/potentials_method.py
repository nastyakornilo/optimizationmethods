import numpy as np
import time


class TransportationProblem(object):
	def __init__(self, supply, demand, costs):
		self.supply = supply
		self.demand = demand
		self.costs = costs
		self.production_points_number = len(self.supply)
		self.consumption_points_number = len(self.demand)
		self.balance()

		shape = (self.production_points_number, self.consumption_points_number)
		self.shipments = np.matrix(np.zeros(shape))
		self._basis_cells = []
		self.nonbasis_cells = []

	@property
	def basis_cells(self):
		return self._basis_cells

	@basis_cells.setter
	def basis_cells(self, new_basis):
		self._basis_cells = new_basis
		self.nonbasis_cells = []
		for i in range(self.production_points_number):
			for j in range(self.consumption_points_number):
				if (i, j) not in self.basis_cells:
					self.nonbasis_cells.append((i,j))
	
		# self.A_basis = self.A[:,self.basis]
		# print 'A_basis', self.A_basis
		# self.inversed_A_basis =  np.linalg.inv(self.A_basis)
		# print 'inversed_A_basis', self.inversed_A_basis



	#the task must satisfy balance condition -> let's balance
	def balance(self):
		total_supply = sum(self.supply)
		total_demand = sum(self.demand)

		if total_demand == total_supply:
			return
		if total_demand > total_supply:
			self.add_production_fictitious_point(total_demand - total_supply)
		else:
			self.add_consumption_fictitious_point(total_supply - total_demand)

	def add_production_fictitious_point(self, point_supply):
		self.supply.append(point_supply)
		newrow = np.zeros((1, self.costs.shape[1])) #[0]*self.costs.shape[1]  #
		self.costs = np.concatenate((self.costs,newrow), axis=0)
		self.production_points_number += 1

	def add_consumption_fictitious_point(self, point_demand):
		self.demand.append(point_demand)
		newcol = np.zeros((self.costs.shape[0],1))  #np.array([0]*self.costs.shape[0]).T   # 
		self.costs = np.concatenate((self.costs, newcol), axis=1)  #np.append(, newcol)
		self.consumption_points_number += 1

	def north_west_corner_rule(self):
		#for i in range(self.production_points_number):
		#	for j in range(self.consumption_points_number):
		i, j = 0, 0
		while (i<self.production_points_number and j<self.consumption_points_number):
			self.shipments[i,j] = min(self.supply[i], self.demand[j])
			self.supply[i] -= self.shipments[i,j]
			self.demand[j] -= self.shipments[i,j]
			self.basis_cells.append((i,j))

			if self.supply[i] == 0:
				i += 1
			else:
				j += 1		
#SHIT !!!!!!!!!!!!!!!!!!!!!!!
		self.basis_cells = self.basis_cells

	def find_entering_cell(self):
		u, v = self.find_potentials()
		
		i, j = self.nonbasis_cells[0]
		min_delta = 0
		entering_cell = None

		neg_deltas = []
		deltas  = []

		print 'potentials\n', u, v
		# return all()
		for i, j in self.nonbasis_cells:
			
			# print 'i, j', i, j
			# print 'u+v', u[i]+v[j], 'cost',  self.costs[i, j]
			
			delta = self.costs[i, j] - u[i]-v[j]
			deltas.append((delta, (i, j)))
			if delta < 0:
				neg_deltas.append((delta, (i, j)))
				if delta < min_delta:
					entering_cell = (i, j)
					min_delta = delta
		print 'deltas', deltas
		print 'neg_deltas', neg_deltas
		return entering_cell



	# def find_etimates(self):

	def find_potentials(self):
		u = np.zeros(self.production_points_number)
		# v = np.zeros(self.consumption_points_number)

		vars_number = self.production_points_number + self.consumption_points_number

		A = np.zeros((vars_number, vars_number))
		b = np.zeros(vars_number)
		u_0_pos = 0
		A[0][u_0_pos], b[0] = 1, 0

		for ind, (i, j) in enumerate(self.basis_cells):
			A[ind+1][i] = 1
			A[ind+1][len(u)+j] = 1
			b[ind+1] = self.costs[i, j]

		# print A
		# print b

		potentials = np.linalg.solve(A, b)

		u = potentials[:len(u)]
		v = potentials[len(u):]

		#u[i] = 0
		# i, j = self.basis_cells[0]
		# print 'i', i
		# v[j] = self.costs[i, j] - u[i]

		# for cell_number in xrange(1, len(self.basis_cells)):
		# 	prev_cell_i, prev_cell_j = self.basis_cells[cell_number-1]
		# 	i, j = self.basis_cells[cell_number]

		# 	if prev_cell_i == i:
		# 		v[j] = self.costs[i, j] - u[i]
		# 	else:    #prev_cell_j == j
		# 		u[i] = self.costs[i, j] - v[j]

		return u, v

	def find_cycle(self, cells):  #by strikeout method
		finished = False

		rows = [i for i in xrange(self.production_points_number)]
		cols = [j for j in xrange(self.consumption_points_number)]

		while (not finished) and (cells != []):
			finished = True

			# print cells

			for i in rows:
				i_row_cells = [ind for ind, cell in enumerate(cells) if cell[0]==i]
				if len(i_row_cells) == 1:
					i_row_cell = i_row_cells[0]
					cells = cells[:i_row_cell]+ cells[i_row_cell+1:]
					finished = False
			
			for j in cols:
				j_col_cells = [ind for ind, cell in enumerate(cells) if cell[1]==j]
				if len(j_col_cells) == 1:
					j_col_cell = j_col_cells[0]
					cells = cells[:j_col_cell]+ cells[j_col_cell+1:]
					finished = False
		return cells


	def update_shipments_on_cycle_cells(self, step, increase_cells, decrease_cells):
		print 'STEP!!!', step
		for cell in increase_cells:
			self.shipments[cell] = self.shipments[cell] + step

		for cell in decrease_cells:
			self.shipments[cell] = self.shipments[cell] - step

	def update_basis_cells(self, leaving_cell, entering_cell):

		# pos = np.where(self.basis_cells == leaving_cell)
		pos = self.basis_cells.index(leaving_cell)
		self.basis_cells[pos] = entering_cell
#SHIT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		self.basis_cells = self.basis_cells

		# time.sleep(10)

	def split_cycle_cells(self, cells):  #split on positive and negative
		increase_cells = [cells[0]]
		decrease_cells = []
		current_positive = False
		was_changed = False
		curr_i, curr_j = cells[0]
		while True:
			was_changed = False
			for cell in cells:
				cell_is_already_used = cell in increase_cells or cell in decrease_cells
				differ_by_one_coordinate = ((cell[0]==curr_i and cell[1]!=curr_j)
											or (cell[0]!=curr_i and cell[1]==curr_j))
				can_be_next = not cell_is_already_used and differ_by_one_coordinate
				if can_be_next:
					if current_positive:
						increase_cells.append(cell)
					else:
						decrease_cells.append(cell)
					current_positive = not current_positive
					curr_i = cell[0]
					curr_j = cell[1]
					was_changed = True
			if not was_changed:
				return increase_cells, decrease_cells





	def solve(self):
		self.north_west_corner_rule()
		# self.shipments = np.matrix([
		# [55, 0, 0, 0, 75, 0],
		# [55, 0, 0, 0, 0, 0],
		# [20, 60, 0, 0, 0, 0],
		# [0, 15, 50, 0, 0, 0],
		# [0, 0, 15, 60, 0, 60]
		# ])

		# self.basis_cells = [(0, 4), (0,0), (1, 0), (2, 0), (2, 1), (3,1), (3, 2), (4,2), (4, 3), (4, 5)]

		# self.basis_cells = [(4, 5), (4, 3), (4,2), (3, 2), (3,1), (2, 1), (2, 0), (1, 0), (0,0), (0, 4) ]

		print 'init plan'
		print self.shipments
		print self.basis_cells

		while True:

			print 'iteration----------------------------------------'
			entering_cell = self.find_entering_cell()
			print 'enter cell ', entering_cell

			if entering_cell is None:
				# current_plan_is_optimal
				return self.shipments, self.basis_cells
			
			cycle_cells = self.find_cycle([entering_cell] + self.basis_cells)
			print 'cycle_cells', cycle_cells

			increase_cells, decrease_cells = self.split_cycle_cells(cycle_cells)

			# #split
			# for i, cell in enumerate(cycle_cells):
			# 	if i & 1 == 0:
			# 		increase_cells.append(cell)
			# 	else:
			# 		decrease_cells.append(cell)

			#find step
			print 'decrease cells', decrease_cells
			print 'increase cells', increase_cells

#SHIT
			decreasing_shipments = [self.shipments[cell] for cell in decrease_cells]
			# print 'decreasing_shipments', decreasing_shipments
			step = np.min(decreasing_shipments)
			leaving_cell = decrease_cells[np.argmin(decreasing_shipments)]
			print 'leaving cell', leaving_cell


			self.update_shipments_on_cycle_cells(step, increase_cells, decrease_cells)
			self.update_basis_cells(leaving_cell, entering_cell)



			print 'shipments \n', self.shipments
			print 'basis cells',self.basis_cells
			# print 'non basic cells', self.nonbasis_cells


			# m = np.multiply(self.costs, self.shipments)

			# print 'multiplication: ', m
			# print 'object func', m.sum()
			# time.sleep(1)




def print_results(supply, demand, costs):
	problem = TransportationProblem(supply, demand, costs)
	# print 'costs\n', problem.costs
	# print 'supply\n', problem.supply
	# print 'demand\n', problem.demand

	x, J_b = problem.solve()
	print 'RESULT'
	print 'basis\n', problem.basis_cells
	print 'shipments\n', problem.shipments
	# print 'costs', problem.costs

	# m = 

	# print 'multiplication: ', m
	print 'object func', np.multiply(problem.costs, x).sum()
	

def task1():
	supply = [20,11, 18, 27]
	demand = [11,4,10,12,8,9,10,4]
	costs = np.matrix([
		[-3, 6, 7, 12, 6, -3, 2, 16],
		[4, 3, 7, 10, 0, 1, -3, 7],
		[19, 3, 2, 7, 3, 7, 8,  15],
		[1, 4, -7, -3, 9, 13, 17, 22]
		])
	print_results(supply, demand, costs)

def task2():
	supply = [15,12, 18, 20]
	demand = [5,5,10,4,6,20,10,5]
	costs = np.matrix([
		[-3, 10, 70, -3, 7, 4, 2, -20],
		[3, 5, 8, 8, 0, 1, 7, -10],
		[-15, 1, 0, 0, 13, 5, 4,  5],
		[1, -5, 9, -3, -4, 7, 16, 25]
		])
	print_results(supply, demand, costs)	

def example1():
	supply = [20,30, 25]
	demand = [10,10,10,10,10]
	costs = np.matrix([
		[2, 8, -5, 7, 10],
		[11, 5, 8, -8, -4],
		[1, 3, 7, 4, 2]
		])
	print_results(supply, demand, costs)

def math_semestr_example():
	supply = [130,55, 80, 65, 135]
	demand = [130, 75, 65, 60, 75, 60]
	costs = np.matrix([
		[6, 6, 8, 5, 4, 3],
		[2, 4, 3, 9, 8, 5],
		[3, 5, 7, 9, 6, 11],
		[3, 5, 4, 4, 2, 1],
		[2, 5, 6, 3, 2, 8]
		])
	print_results(supply, demand, costs)

if __name__ == '__main__':
	# example1()
	# task1()
	task2()
	# math_semestr_example()