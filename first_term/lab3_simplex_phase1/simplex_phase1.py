import numpy as np
from simplex_phase2 import phase2


def check_original_problem_is_unfeasible(artificial_x):
	return any(x!=0 for x in artificial_x)


def find_leaving_index(basis_indices, real_var_number):
	artifitial_indices = np.array([i for i in basis_indices if i>=real_var_number])
	return np.min(artifitial_indices)

def find_entering_index(real_var_number, basis_indices, A, pos):
	#pos is position for entering index in J_b
	inversed_basis_A = np.linalg.inv(A[:,basis_indices])
	for j in range(real_var_number):
			if j not in basis_indices:
				if np.dot(inversed_basis_A, A[:,j])[pos] != 0:
					return j

def make_b_positive(A, b):
	for j in range(len(b)):
		if b[j]<0:
			b[j] = -b[j]
			A[j] = -A[j]
	return A, b

def is_original_problems_bfs(real_var_number, basis_indices):
	return all(i<real_var_number for i in basis_indices)

def get_auxiliary_task(A,b):
	
	artificial_var_number = A.shape[0]   #number of row
	real_var_number = A.shape[1]    #number of columns
	x = np.array([0] * real_var_number +b)
	c = np.matrix([[0]]*real_var_number + [[-1]] * artificial_var_number)
	basis_indices = np.arange(real_var_number, real_var_number+ artificial_var_number)
	
	extended_A = np.concatenate([A, np.eye(artificial_var_number)], axis=1)

	return c, extended_A, b, x, basis_indices
	
def phase1(A, b, eps=10**(-6)):
	A, b = make_b_positive(A, b)
	artificial_var_number = A.shape[0]   #number of row
	real_var_number = A.shape[1]    #number of columns
	c, extended_A, b, x, basis_indices = get_auxiliary_task(A,b)

	status_res, x, basis_indices = phase2(c, extended_A, b, x, basis_indices, eps)

	if check_original_problem_is_unfeasible(x[real_var_number:]):
		raise Exception('Problem is unfeasible')

	while True:
		if is_original_problems_bfs(real_var_number, basis_indices):  # basis indices contain  indices of real vars
			original_problem_bfs = x[:real_var_number], basis_indices
			return original_problem_bfs

		leaving_index = find_leaving_index(basis_indices, real_var_number)
		pos  = list(basis_indices).index(leaving_index)   #index_of_leaving_basis_index

		#non_basis_real_var_indices = [i for i in range(real_var_number)if i not in basis_indices]
		entering_index = find_entering_index(real_var_number, basis_indices, extended_A, pos)
		if entering_index is None:
			row_to_del = leaving_index - real_var_number
			#print('row to delete ', row_to_del)
			A = np.delete(A, (row_to_del), axis=0)
			del b[row_to_del]
			#print (A,b)
			# real_var_number -= 1    #number of columns
			return phase1(A,b,eps)
		
		basis_indices[pos] = entering_index



def main():
	A = np.matrix([[1.,1.,1.],[1.,1.,1.], [2.,2.,2.]])
	b = [0.,1.,0.]
	# print()
	x, J_b = phase1(A,b)
	print('BFS:')
	print('x', x)
	print('J_b', J_b)


if __name__ == "__main__":
	main()