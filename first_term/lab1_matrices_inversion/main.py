import numpy as np


def main():
    dim = 3
    A=np.matrix([[1, -1, 0], [0, 1, 0],[0, 0, 1]])  #np.zeros((dim, dim))
    inversed_A = np.matrix([[1, 1, 0], [0, 1, 0], [0, 0, 1]])
    x = np.matrix([[0], [0],[0]])
    i = 3
    #B - matrix A with i column changed on x

    l = np.dot(inversed_A, x)
    if l[i-1] == 0:
        raise Exception ("Can not inverse matrix")
    l1 = l.copy()
    l1[i-1] = -1

    l2 = np.dot(-1/l[i-1, 0],l1)

    Q = np.eye(dim)
    Q[:, i - 1] = l2.transpose()


    inversed_B = np.dot(Q,inversed_A)
    print (inversed_B)

if __name__ =='__main__':
    main()



