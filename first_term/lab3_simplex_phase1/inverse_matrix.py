import numpy as np

#B - matrix A with i column changed on x
def inv(A, inversed_A, x, i):
    dim = len(A)
    l = np.dot(inversed_A, x)
    # print(inversed_A, x)
    # print ('l',l)
    if l[i] == 0:
        raise Exception ("Can not inverse matrix")
    l1 = l.copy()
    l1[i] = -1

    l2 = np.dot(-1/l[i, 0],l1)

    Q = np.eye(dim)
    Q[:, i] = l2.transpose()

    inversed_B = np.dot(Q,inversed_A)
    return inversed_B


def main():
    dim = 3
    A=np.matrix([[1, 0, 0], [0, 1, 0],[0, 0, 1]])  #np.zeros((dim, dim))
    inversed_A = np.matrix([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    x = np.matrix([[-1], [1],[0]])
    i = 1
    #B - matrix A with i column changed on x

    
    inversed_B = inv(A, inversed_A, x, i)
    print (inversed_B)

if __name__ =='__main__':
    main()



