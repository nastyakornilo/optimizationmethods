import numpy as np
#import math


class DualSimplexSolver(object):
	def __init__(self):
		self.A = None
		self.b = None
		self.cost_vector = None
		self._basis = None    #field
		self.nonbasis = None
		self.A_basis = None
		self.inversed_A_basis =  None

	@property
	def basis(self):
		return self._basis

	@basis.setter
	def basis(self, new_basis):
		self._basis = new_basis
		print 'basis', new_basis
		indices = xrange(self.A.shape[1])
		self.nonbasis = [i for i in indices if i not in self.basis]
		self.A_basis = self.A[:,self.basis]
		print 'A_basis', self.A_basis
		self.inversed_A_basis =  np.linalg.inv(self.A_basis)
		print 'inversed_A_basis', self.inversed_A_basis


	def build_pseudoplan(self):
		pseudoplan = np.zeros((self.A.shape[1], 1))  #[np.newaxis].T
		pseudoplan[self.basis] = np.dot(self.inversed_A_basis, self.b)
		# print 'pseudoplan', pseudoplan
		return pseudoplan

	def improve_plan(self, pseudoplan):
		leaving_var_ind = self.select_leaving_var_index(pseudoplan)
		pos = np.where(self.basis == leaving_var_ind)     #position in basis of leaving variable index
		
		psi = self.inversed_A_basis[pos].T
		steps = self.find_steps(psi)
		step = steps.min()

		if step==float('inf'):   #dual is unbounded -> primal is infeasible
			raise Exception('Problem is infeasible')
		
		entering_var_ind = self.select_entering_var_index(steps)

		self.change_basis(pos, entering_var_ind)
		self.change_y(step*psi)

		# return new_y, new_basis

	def select_leaving_var_index(self, pseudoplan):
		print 'argmin $ ', np.argmin(pseudoplan) 
		return np.argmin(pseudoplan)  #[0]

	def find_steps(self, psi):
		nu = []
		transposed_psi =psi.T
		for j in range(self.A.shape[1]):
			nu.append(transposed_psi *  self.A[:,j])

		# if all(nu_j>=0 for nu_j in nu)
		#     raise Exception('Problem is infeasible')

		steps = np.array([float('inf')] * self.A.shape[1])
		for j in self.nonbasis:
			if nu[j]<0:
				steps[j] = (self.cost_vector[j] - np.dot(self.A[:,j].T, self.y)) / nu[j]

		return steps

	def select_entering_var_index(self, steps):
		print 'argmin ', np.argmin(steps)
		return np.argmin(steps)  #[0]

	def change_basis(self, pos, entering_var_ind):
		new_basis = self.basis
		new_basis[pos] = entering_var_ind
		self.basis = new_basis

	def change_y(self, delta_y):
		self.y = self.y + delta_y

	def solve(self, A, b, cost_vector, basis):
		self.A = A
		self.b = b
		self.cost_vector = cost_vector
		self.basis = basis   #property  - autoupdate nonbasis , A_basis, inversed_A_basis
		self.y = np.dot(cost_vector[basis].T, self.inversed_A_basis).T

		while True:
			#check is we found the solution 
			pseudoplan = self.build_pseudoplan()
			if self.is_optimal_for_primal_task(pseudoplan):
				return pseudoplan, self.basis

			# WHAT WE ARE IMPROVE Y OR PSEUDOPLAN?????
			self.improve_plan(pseudoplan)

	def is_optimal_for_primal_task(self, pseudoplan):
		return all(x>=0 for x in pseudoplan)


def example1():
	A = np.matrix(
		[
			[-2., -1., 1., -7., 0., 0., 0., 2.],
			[4., 2., 1., 0., 1.,  5., -1., -5.],
			[1., 1., 0., -1., 0., 3., -1.,  1.]
		])
	c = np.array([2., 2., 1., -10., 1., 4., -2., -3.])
	b = np.array([[-2.], [4.], [3.]])
	J_b = np.array([1, 4, 6])
	solver = DualSimplexSolver()
	print solver.solve(A, b, c, J_b)

# def example2():
# 	A = np.matrix(
# 	[
# 		[-2., -1., 1., -7., 0., 0., 0., 2.],
# 		[4., 2., 1., 0., 1.,  5., -1., -5.],
# 		[1., 1., 0., 1., 0., 3., 1.,  1.]
# 	])
# 	c = np.array([2., 2., 1., -10., 1., 4., 0., -3.])
# 	# b = np.array([-2., 4., 3.])[np.newaxis].T
# 	b = np.matrix([[-2.], [4.], [3.]])
# 	J_b = np.array([1, 4, 6])
# 	solver = DualSimplexSolver()
# 	print solver.solve(A, b, c, J_b)

def task3():
	A = np.matrix(
		[
			[-2., -1., 1., -7., 1., 0., 0., 2.],
			[-4., 2., 1., 0., 5.,  1., -1., 5.],
			[1., 1., 0., 1., 4., 3., 1.,  1.]
		])
	c = np.array([12., -2., -6., 20., -18., -5., -7., -20.])
	b = np.matrix([[-2.], [8.], [-2.]])
	J_b = np.array([1, 3, 5])
	solver = DualSimplexSolver()
	print solver.solve(A, b, c, J_b)



if __name__ == '__main__':
	task3()
	#example1()