import numpy as np
from scipy.optimize import linprog, OptimizeResult
import math
import copy


class IntegerLpProblem(object):
    def __init__(self, c=None, A=None, b=None, bounds=None):
        self.c = c
        self.A = A
        self.b = b
        self.bounds = bounds


def solve_integer_lp(int_lp_problem, eps=10**(-10)):
    record = -float('infinity')
    x = None
    stack = [int_lp_problem]

    while stack:
        problem = stack.pop()
        optimize_res = linprog(-problem.c, A_eq=problem.A, b_eq=problem.b, bounds=problem.bounds)
        if optimize_res.success and -optimize_res.fun > record:
            non_integer_pos = find_non_integer_component_pos(optimize_res.x, eps)
            if non_integer_pos is None:
                record = -optimize_res.fun   # -fun because we solve lp task with -c (because we minimize func in lp, but in original task maximize)
                x = optimize_res.x
            else:
                non_integer_component_value = optimize_res.x[non_integer_pos]
                subproblems = split_problem(problem, non_integer_pos, non_integer_component_value, eps)

                stack.extend(subproblems)

    #  TODO: add the case when all problems are infeaseable
    #  What if problem is unbounded
    return record, x


def find_non_integer_component_pos(x, eps):
    for i, component in enumerate(x):
        e = abs(math.floor(component) - component)
        if abs(round(component) - component) > eps:
            return i


def split_problem(problem, non_integer_pos, non_integer_component_value, eps):
    problem1 = copy.deepcopy(problem)    #  OR DEEP COPY?????????????
    problem2 = copy.deepcopy(problem)

    floor_x = math.floor(non_integer_component_value)

    problem1.bounds[non_integer_pos][1] = floor_x
    problem2.bounds[non_integer_pos][0] = floor_x + 1

    valid_subproblems = []

    for problem in [problem1, problem2]:
        if problem.bounds[non_integer_pos][0] <= problem.bounds[non_integer_pos][1]:
            valid_subproblems.append(problem)

    return valid_subproblems


def example1():
    problem = IntegerLpProblem()
    problem.c = np.array([7, -2, 6, 0, 5, 2])          #  np.matrix([[7], [-2], [6], [0], [5], [2]])
    problem.A = np.matrix([[1, -5, 3, 1, 0, 0], [4, -1, 1, 0, 1, 0], [2, 4, 2, 0, 0, 1]])
    problem.b = np.matrix([[-8], [22], [30]])
    problem.bounds = np.array([(2, 6), (1, 6), (0, 5), (0, 2), (1, 4), (1, 6)])
    print(solve_integer_lp(problem))


def example2():
    problem = IntegerLpProblem()
    problem.c = np.array([7, -2, 6, 0, 5, 2])  # np.matrix([[7], [-2], [6], [0], [5], [2]])
    problem.A = np.matrix([[1, 0, 3, 1, 0, 0], [0, -1, 1, 1, 1, 2], [-2, 4, 2, 0, 0, 1]])
    problem.b = np.matrix([[10], [8], [10]])
    problem.bounds = np.array([(0, 3), (1, 3), (-1, 6), (0, 2), (-2, 4), (1, 6)])
    print(solve_integer_lp(problem))


def example3():
    problem = IntegerLpProblem()
    problem.c = np.array([-3, 2, 0, -2, -5, 2])  # np.matrix([[7], [-2], [6], [0], [5], [2]])
    problem.A = np.matrix([[1, 0, 1, 0, 0, 1], [1, 2, -1, 1, 1, 2], [-2, 4, 1, 0, 1, 0]])
    problem.b = np.matrix([[-3], [3], [13]])
    problem.bounds = np.array([(-2, 2), (-1, 3), (-2, 1), (0, 5), (1, 4), (-4, 1)])
    print(solve_integer_lp(problem))


def task1():
    problem = IntegerLpProblem()
    problem.c = np.array([2, 1, -2, -1, 4, -5, 5, 5])  # np.matrix([[7], [-2], [6], [0], [5], [2]])
    problem.A = np.matrix([[1, 0, 0, 12, 1, -3, 4, -1],
                           [0, 1, 0, 11, 12, 3, 5, 3],
                           [0, 0, 1, 1, 0, 22, -2, 1]])
    problem.b = np.matrix([[40], [107], [61]])
    problem.bounds = np.array([(0, 3), (0, 5), (0, 5), (0, 3), (0, 4), (0, 5), (0, 6), (0, 3)])
    print(solve_integer_lp(problem))


if __name__ == '__main__':
    # example3()
    task1()