import numpy as np
import time


eps = 10**(-14)
DIRECTION_ACCURACY = 15
class QuadraticProgrammingProblem(object):
	def __init__(
		self, 
		constraints_matrix, 
		obj_func_coefficients,
		Hessian,
		RHS
		):
		self.A = constraints_matrix #constraints matrix
		self.obj_func_coeffs = obj_func_coefficients
		self.Hessian = Hessian
		self.RHS = RHS  #right-hand-sides


class QPSolver(object):
	def __init__(self):
		self.qp = None  #QuadraticProgrammingProblem
		self.x = None  #decision_vars
		#what is active set and what is working set around next properties
		self.J_op = None   #more probably that it is active set
		self.J_op_extended = None

		self._direction = None
		self._deltas = None
		self._blocking_constraint_ind = None

		self._inversed_A_op = None

	# @property
	# def direction(self):
	# 	return self._direction

	# @property
	# def deltas(self):
		#return self._deltas

	@property
	def KKT_matrix(self):
		# print 'ext', self.J_op_extended
		sub_Hessian = self.qp.Hessian[self.J_op_extended][:, self.J_op_extended]
		sub_A = self.qp.A[:,self.J_op_extended]
		# print sub_Hessian
		# print sub_A.T
		# print 'rows ', len(sub_Hessian), ' rows ', len(sub_A)
		# print 'total rows ', len(sub_Hessian)+len(sub_A)
		zeros = np.zeros((sub_A.shape[0], sub_A.T.shape[1]))
		left = np.concatenate((sub_Hessian, sub_A), axis=0)
		# print left
		right = np.concatenate((sub_A.T, zeros), axis=0)
		# print left
		# print right
		KKT = np.concatenate((left, right), axis=1)
		# print 'KKT'
		# print KKT
		return KKT

	#properties: KKT_matrix, direction, deltas

	def find_deltas(self):
		# print self.qp.obj_func_coeffs.T
		# print self.x.T
		c = self.qp.obj_func_coeffs.T + np.dot(self.x.T, self.qp.Hessian)
		print 'c', c
		print 'c_op ', c[:,self.J_op]
		print 'A_op\n ',self.qp.A[:, self.J_op]
		print 'inversed_A_op\n',self._inversed_A_op
		potentials = -np.dot(c[:,self.J_op], self._inversed_A_op)
		# print len(potentials)
		print 'potentials:', potentials
		deltas = np.dot(potentials, self.qp.A) + c
		deltas = list(deltas.flat)
		# deltas = [round(x, 13) for x in deltas]
		# print deltas
		for i in self.J_op_extended:
			# if abs(deltas[i]) > eps:
			# 	print "PROOOOOOOOOOOOOBLEM: ", i, deltas[i]
			# 	time.sleep(3)
		 	deltas[i] = 0
		# # print potentials
		# print self.qp.A
		# print np.dot(potentials, self.qp.A)
		# print deltas.T
		return deltas   #.T

	def find_direction(self, delta_below_zero_ind):
		direction = np.zeros(len(self.x))
		direction[delta_below_zero_ind] = 1
		
		#find direction components at J_op positions
		print self.KKT_matrix
		inversed_KKT = np.linalg.inv(self.KKT_matrix)

		sub_Hessian =self.qp.Hessian[self.J_op_extended][:,delta_below_zero_ind]
		print sub_Hessian

		sub_A= self.qp.A[:,delta_below_zero_ind]
		# print 'rows2 ', len(sub_Hessian), ' rows ', len(sub_A)
		# print 'total rows ', len(sub_Hessian)+len(sub_A)
		# what the meaning of b_asterisk....?
		b_asterisk = np.concatenate(
			(self.qp.Hessian[self.J_op_extended][:, delta_below_zero_ind],
			self.qp.A[:,delta_below_zero_ind]),
			axis=0)
		b_asterisk = -b_asterisk
		# print'j0'
		# print delta_below_zero_ind
		print 'b*'
		print b_asterisk
		print 'H x b*'
		print list(np.dot(inversed_KKT, b_asterisk).flat)[:len(self.J_op_extended)]
		# print self.J_op_extended
		# print sub_Hessian
		# print 'mnnn', self.qp.Hessian[1, delta_below_zero_ind]
		# print list(np.dot(inversed_KKT, b_asterisk).flat)[:len(self.J_op)]
		direction[self.J_op_extended] = list(np.dot(inversed_KKT, b_asterisk).flat)[:len(self.J_op_extended)]
		direction = np.array([round(x, DIRECTION_ACCURACY) for x in direction])
		print direction[self.J_op_extended]
		print 'direction', direction
		return direction

	def find_delta_below_zero_ind(self):
		print 'deltas', self._deltas
		min_delta = 0
		pos = None
		# print 'DELTAS', self._deltas
		for i, delta in enumerate(self._deltas):
			# print i, delta, min_delta
			if delta<min_delta:  # and i not in self.J_op_extended: <- already true #delta-min_delta<eps:
				min_delta = delta
				pos = i
		return pos

	def solve(self, qp, decision_vars, J_op, J_op_extended):
		self.qp = qp
		self.x = decision_vars
		self.J_op = J_op
		self.J_op_extended = J_op_extended

		# iter = 1

		while True:	
			self._inversed_A_op = np.linalg.inv(self.qp.A[:, self.J_op])

			self._deltas = self.find_deltas()
			#index where delta < 0
			j_0 = self.find_delta_below_zero_ind()#(deltas)
			print 'j_0', j_0
			# iter += 1


			plan_is_optimal = j_0 is None

			if plan_is_optimal:
				# J = [j for j in range(len(self.x)) if j not in self.J_op_extended] 
				# for j in J:
				# 	self.x[j] = 0
				return self.x, self.J_op, self.J_op_extended
			
			self._direction = self.find_direction(j_0) #  must update on update j_0
			self._steps = self.find_steps(j_0)

			# step = self.find_(j_0)
			#blocking constraint - j_asterisk
		

			if self.max_feasible_step == float('infinity'):
				raise Exception('Object function is unbounded')

			
			self.update_plan(j_0)

			print 'J_op', self.J_op
			print 'J_op_extended',self.J_op_extended
			print ['-']*10


	def find_steps(self, j_0):
		steps = np.zeros(len(self.J_op_extended)+1)
		inf = False
		for pos, j in enumerate(self.J_op_extended):
			if self._direction[j] >= 0:  #self._direction[j] >= -eps: 
				steps[pos] = float('infinity')
				inf = True
			else:
				steps[pos] = -self.x[j]/self._direction[j] 

		psi = np.dot(
			np.dot(self._direction, self.qp.Hessian), 
			self._direction.T
			)
		
		if psi>0: #psi>-eps:
			steps[len(steps)-1] = abs(self._deltas[j_0])/psi
		elif psi == 0: #abs(psi)<=eps:
			steps[len(steps)-1] = float('infinity')
			inf = True
		# if inf:
		# 	print 'steps ', steps
		# 	time.sleep(3)

		self.max_feasible_step = min(steps)
		
		#find blocking_constraint_ind
		if self.max_feasible_step == steps[len(steps)-1]:
			self._blocking_constraint_ind = j_0
		else:
			pos_in_J_op = (list(steps)).index(
				self.max_feasible_step
				)
			self._blocking_constraint_ind = self.J_op_extended[pos_in_J_op]
		print 'j*', self._blocking_constraint_ind

		return steps


	def update_plan(self, j_0):
		print 'steps', self._steps
		print 'step', self.max_feasible_step
		#print 'direction', self._direction
		self.x = self.x + self.max_feasible_step*self._direction

		print 'x', self.x
		
		if j_0 == self._blocking_constraint_ind:
			self.J_op_extended.append(self._blocking_constraint_ind)
			# print 'case 1'
			# time.sleep(3)
			return

		J_difference = [j for j in self.J_op_extended if j not in self.J_op]
		
		if self._blocking_constraint_ind in J_difference:
			self.J_op_extended.remove(self._blocking_constraint_ind)

			# print 'case 2'
			# time.sleep(3)
			return

		pos = list(self.J_op).index(self._blocking_constraint_ind)

		for j in J_difference:
			if np.dot(self._inversed_A_op, self.qp.A[:, j])[pos]!=0:  #abs(np.dot(self._inversed_A_op, self.qp.A[:, j])[pos])>eps: #
				entering_j = j
				self.J_op_extended.remove(self._blocking_constraint_ind)
				self.J_op.remove(self._blocking_constraint_ind)
				self.J_op.insert(pos, j)
				# break

				# print 'case 3'
				# time.sleep(3)
				return


		exists = pos !=-1  #False
		print exists

		# for j in J_difference:
			# if abs(np.dot(self._inversed_A_op, self.qp.A[:, j])[pos])<=eps:
			# exists = True
			# break
		#if set(self.J_op) == set(self.)
		if (self.J_op == self.J_op_extended or exists):
			self.J_op.remove(self._blocking_constraint_ind)
			self.J_op.insert(pos, j_0)

			self.J_op_extended.remove(self._blocking_constraint_ind)
			self.J_op_extended.insert(pos, j_0)

			# print 'case 4'
			# time.sleep(3)
			# return
		# print 'NO UPDATE !!!!!!'
		# time.sleep(10)



def print_task_res(qp, decision_vars, J_op, J_op_extended,
	obj_func_coefficients, Hessian, RHS):
	solver = QPSolver()
	x, J_op, J_op_extended = solver.solve(qp, decision_vars, J_op, J_op_extended)
	print x, J_op, J_op_extended
	obj_func_val = np.multiply(obj_func_coefficients.T, x ).sum()
	# print
	obj_func_val += 0.5* np.dot(np.dot(x.T, Hessian),x)
	print 'OBJ FUNC VALUE: ', obj_func_val


def example():
	constraints_matrix = np.matrix([
			[1., 2., 0., 1., 0., 4., -1., -3.],
			[1., 3., 0., 0., 1., -1., -1., 2.],
			[1., 4., 1., 0., 0., 2., -2., 0.]
		]) 
	obj_func_coefficients = np.array(
		[-10., -31., 7., 0., -21., -16., 11., -7.]
		)[np.newaxis, :].T
	Hessian = np.matrix([
		[6., 11., -1., 0., 6., -7., -3., -2.],
		[11., 41., -1., 0., 7., -24., 0., -3.],
		[-1., -1., 1., 0., -3., -4., 2., -1.],
		[0., 0., 0., 0., 0., 0., 0., 0.,],
		[6., 7., -3., 0., 11., 6., -7., 1],
		[-7., -24., -4., 0., 6., 42., -7., 10.],
		[-3., 0., 2., 0., -7., -7., 5., -1.],
		[-2., -3., -1., 0., 1., 10., -1., 3.]
		])
	RHS = np.array([4, 5, 6])[np.newaxis, :].T
	qp = QuadraticProgrammingProblem(
		constraints_matrix, 
		obj_func_coefficients,
		Hessian,
		RHS)


	decision_vars = np.array([0., 0., 6., 4., 5., 0., 0., 0.])
	J_op = [2, 3, 4]
	J_op_extended = [2, 3, 4]
	print_task_res(qp, decision_vars, J_op, J_op_extended,
		obj_func_coefficients, Hessian, RHS)
	
def task1():
	constraints_matrix = np.matrix([
			[11., 0., 0., 1., 0., -4., -1., 1.],
			[1., 1., 0., 0., 1., -1., -1., 1.],
			[1., 1., 1., 0., 1., 2., -2., 1.]
		]) 
	B = np.matrix([
		[1., -1., 0., 3., -1., 5., -2., 1.],
		[2., 5., 0., 0., -1., 4., 0., 0.],
		[-1., 3., 0., 5., 4., -1., -2., 1.]
		])
	obj_func_coefficients = -np.dot(np.array([6,10,9]), B).T
	Hessian = np.dot(B.T, B)
	RHS = np.array([4, 5, 6])[np.newaxis, :].T
	qp = QuadraticProgrammingProblem(
		constraints_matrix, 
		obj_func_coefficients,
		Hessian,
		RHS)
	

	decision_vars = np.array([0.7273, 1.2727, 3., 0., 0., 0., 0., 0.])
	J_op = [0, 1, 2]
	J_op_extended = [0, 1, 2]
	print_task_res(qp, decision_vars, J_op, J_op_extended,
		obj_func_coefficients, Hessian, RHS)


def task2():
	constraints_matrix = np.matrix([
			[2., -3., 1., 1., 3., 0., 1., 2.],
			[-1., 3., 1., 0., 1., 4., 5., -6.],
			[1., 1., -1., 0., 1., -2., 4., 8.]
		]) 
	B = np.matrix([
		[1., 0., 0., 3., -1., 5., 0., 1.],
		[2., 5., 0., 0., 0., 4., 0., 0.],
		[-1., 9., 0., 5., 2., -1., -1., 5.]
		])
	obj_func_coefficients = np.array(
		[-13., -217., 0., -117., -27., -71., 18., -99.]
		)[np.newaxis, :].T
	Hessian = np.dot(B.T, B)
	RHS = np.array([8, 4, 14])[np.newaxis, :].T
	qp = QuadraticProgrammingProblem(
		constraints_matrix, 
		obj_func_coefficients,
		Hessian,
		RHS)
	

	decision_vars = np.array([0., 2., 0., 0., 4., 0., 0., 1.])
	J_op = [1, 4, 7]
	J_op_extended = [1, 4, 7]
	print_task_res(qp, decision_vars, J_op, J_op_extended,
		obj_func_coefficients, Hessian, RHS)

def task3():
	constraints_matrix = np.matrix([
			[0., 2., 1., 4., 3., 0., -5., -10.],
			[-1., 3., 1., 0., 1., 3., -5., -6.],
			[1., 1., 1., 0., 1., -2., -5., 8.]
		]) 
	obj_func_coefficients = np.array(
		[1., 3., -1., 3., 5., 2., -2., 0.]
		)[np.newaxis, :].T

	Hessian = np.matrix([
		[1., 0., 0., 0., 0., 0., 0., 0.],
		[0., 1., 0., 0., 0., 0., 0., 0.],
		[0., 0., 0., 0., 0., 0., 0., 0.],
		[0., 0., 0., 1., 0., 0., 0., 0.,],
		[0., 0., 0., 0., 1., 0., 0., 0.],
		[0., 0., 0., 0., 0., 1., 0., 0.],
		[0., 0., 0., 0., 0., 0., 0., 0.],
		[0., 0., 0., 0., 0., 0., 0., 1.]
		])

	print Hessian
	RHS = np.array([6, 4, 14])[np.newaxis, :].T
	qp = QuadraticProgrammingProblem(
		constraints_matrix, 
		obj_func_coefficients,
		Hessian,
		RHS)
	

	decision_vars = np.array([0., 2., 0., 0., 4., 0., 0., 1.])
	J_op = [1, 4, 7]
	J_op_extended = [1, 4, 7]
	print_task_res(qp, decision_vars, J_op, J_op_extended,
		obj_func_coefficients, Hessian, RHS)

def task4():
	constraints_matrix = np.matrix([
			[0., 2., 1., 4., 3., 0., -5., -10.],
			[-1., 1., 1., 0., 1., 1., -1., -1.],
			[1., 1., 1., 0., 1., -2., -5., 8.]
		]) 
	obj_func_coefficients = np.array(
		[1., -3., 4., 3., 5., 6., -2., 0.]
		)[np.newaxis, :].T

	Hessian = np.matrix([
		[25., 10., 0., 3., -1., 13., 0., 1.],
		[10., 45., 0., 0., 0., 20., 0., 0.],
		[0., 0., 20., 0., 0., 0., 0., 0.],
		[3., 0., 0., 29., -3., 15., 0., 3.,],
		[-1., 0., 0., -3., 21., -5., 0., 0.],
		[13., 20., 0., 15., -5., 61., 0., 5.],
		[0., 0., 0., 0., 0., 0., 20., 0.],
		[1., 0., 0., 3., -1., 5., 0., 21.]
		])

	RHS = np.array([20, 1, 7])[np.newaxis, :].T
	qp = QuadraticProgrammingProblem(
		constraints_matrix, 
		obj_func_coefficients,
		Hessian,
		RHS)
	

	decision_vars = np.array([3., 0., 0., 2., 4., 0., 0., 0.])
	J_op = [0, 3, 4]
	J_op_extended = [0, 3, 4]
	print_task_res(qp, decision_vars, J_op, J_op_extended,
		obj_func_coefficients, Hessian, RHS)



if __name__ == '__main__':
	# main()
	task3()
	# example()